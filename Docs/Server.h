#pragma once
#include "Helper.h"
#include <WinSock2.h>
#include <Windows.h>
#include <exception>
#include <iostream>
#include <string>
#include <queue>
#include <thread>
#include <vector>
#include <fstream>
#include <mutex>
#include <iterator>
#include <deque>
using namespace std;
class Server
{
public:
	Server();
	~Server();
	void serve(int port);
	void sendUpdata();
	void uploadFromData();
	void saveToData();

private:

	void accept();
	void clientHandler(SOCKET clientSocket);

	SOCKET _serverSocket;
	string _data;
	deque<pair<string, SOCKET>> _users;
	vector<thread> _clients;

};
