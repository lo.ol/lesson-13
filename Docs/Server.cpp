#include "Server.h"
mutex usersMutex;
mutex dataMutex;
mutex fileMutex;
condition_variable fileCond;

Server::Server()
{

	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}
	catch (...) {}
}

void Server::serve(int port)
{

	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)

	if (::bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;
	thread fileWriting(&Server::saveToData, this);
	fileWriting.detach();
	while (true)
	{
		std::cout << "Waiting for client connection request" << std::endl;
		accept();
	}
}



void Server::saveToData()
{
	while (true)
	{


		unique_lock<mutex> locker(fileMutex);
		fileCond.wait(locker);
		cout << "saving to shared_docs.txt" << endl;

		ofstream myfile("shared_docs.txt");
		if (myfile.is_open())
		{
			dataMutex.lock();
			myfile << this->_data;
			dataMutex.unlock();
			cout << "messages saved to shared_docs.txt" << endl;

			myfile.close();
		}
		else cout << "Unable to open file" << endl;
	}
}

void Server::uploadFromData()
{
	cout << "uploading messages from shared_docs.txt" << endl;
	string line, data;
	ifstream myfile;
	myfile.open("shared_docs.txt", ifstream::in | ifstream::out);
	if (myfile.is_open())
	{
		string str((istreambuf_iterator<char>(myfile)), istreambuf_iterator<char>());
		myfile.close();
		dataMutex.lock();
		this->_data = str;
		dataMutex.unlock();
		cout << "messages uploaded" << endl;
	}
	else {//if not sach file
		ofstream myfile("shared_docs.txt");
		if (myfile.is_open())
			myfile.close();
		else cout << "Unable to open file" << endl;
	}
}



void Server::accept()
{
	// notice that we step out to the global namespace
	// for the resolution of the function accept

	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;
	int typeMessage = Helper::getMessageTypeCode(client_socket);
	int userSize = Helper::getIntPartFromSocket(client_socket, 2);
	string userName = Helper::getStringPartFromSocket(client_socket, userSize);
	usersMutex.lock();
	dataMutex.lock();
	
	this->_users.push_back(pair<string, SOCKET>(userName, client_socket));//add new user.


	Helper::sendUpdateMessageToClient(client_socket, this->_data, this->_users[0].first, _users[0].first, this->_users.size());
	usersMutex.unlock();
	dataMutex.unlock();
	this->_clients.push_back(thread(&Server::clientHandler, this, client_socket));
	this->_clients.back().detach();
	

}

void Server::sendUpdata()
{
	string nextUser; int j = 1;
	usersMutex.lock();
	dataMutex.lock();
	if (!this->_users.empty())
	{
		if (this->_users.size() == 1)//if only one user online
			nextUser = this->_users[0].first;
		else
			nextUser = this->_users[1].first;
		for (auto i = this->_users.begin(); i != this->_users.end(); i++, j++) {
			//position by order in deque
			//send to all online users 101 message
			Helper::sendUpdateMessageToClient(i->second, this->_data, this->_users[0].first, nextUser, j);
		}
	}
	
	usersMutex.unlock();
	dataMutex.unlock();
}

void Server::clientHandler(SOCKET clientSocket)
{
	int typeMessage, userSize, msgSize;
	string nextUser, userName;
	bool flag = true;
	int place = -1;
		try
		{
			while (flag)
			{
				typeMessage = Helper::getMessageTypeCode(clientSocket);

				bool stop = false;
				switch (typeMessage)
				{

				case MT_CLIENT_UPDATE:
					msgSize = Helper::getIntPartFromSocket(clientSocket, 5);
					dataMutex.lock();
					if (msgSize)//if data not empty
						this->_data = Helper::getStringPartFromSocket(clientSocket, msgSize);
					dataMutex.unlock();
					sendUpdata();//send 101
					fileCond.notify_one();//save to file
					break;
				case MT_CLIENT_FINISH:
					msgSize = Helper::getIntPartFromSocket(clientSocket, 5);
					dataMutex.lock();
					this->_data = (Helper::getStringPartFromSocket(clientSocket, msgSize));
					dataMutex.unlock();
					//move first user to back of the queue
					usersMutex.lock();
					this->_users.push_back(this->_users.front());
					this->_users.pop_front();
					usersMutex.unlock();
					sendUpdata();//101
					fileCond.notify_one();//save to file
					break;

				case MT_CLIENT_EXIT:
					
					usersMutex.lock();
					
					for (auto i = _users.begin();!stop ||  i != _users.end(); i++,place++)
					{
						if (clientSocket == i->second)
						{
							stop = true;
						}
					}
					
					_users.erase(_users.begin() + place);
					
					
					usersMutex.unlock();
					
					sendUpdata();//101
					flag = false;

					break;
				default:
					break;
				}
			}
			closesocket(clientSocket);

		}
		catch (const std::exception& e)
		{
			closesocket(clientSocket);
			usersMutex.lock();
			this->_users.pop_front();
			usersMutex.unlock();
			sendUpdata();
		}
	
	
}